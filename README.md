# README #

The following describes the LIBFT project which I did during Academy+Plus/Ecole 42

* Version: 1.0 
* [Daniel Cornea](danok.cornea@gmail.com)

Mainly, the project consists of a collection of  C functions. These functions are designed for later use during the Ecole 42 Courses. 

The aim of the project is to teach the student (in this case, me) the underlying subtleties of these functions. 

The functions contained in the .c files were written by me, having as advisors:
 
1. wonderful websites such as DuckDuckGo and StackOverflow

2. my fellow colleagues from Academy+Plus, who also struggled to finish this project

 

For most of the functions the title is self explanatory, for instance, ft_strlen returns the length of a string. Also, you can check the man pages for the majority of these functions by removing the "ft_" part from the name e.g. for ft_strncat check the man page for strncat. If all this fails and you still need more info reach me via e-mail.